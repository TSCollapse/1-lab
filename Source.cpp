#include <iostream>
#include <chrono>

class Timer
{
private:
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;
    std::chrono::time_point<clock_t> m_beg;
public:
    Timer() : m_beg(clock_t::now())
    {}
    void reset()
    {
        m_beg = clock_t::now();
    }
    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


int main()
{
    srand(time(0));
    int n,sum;
    const int N = 100;
    sum = 0;
    int mas[N];
    std::cin >> n;
    for (int i = 0; i < n; i++)
        {
            std::cin >> mas[i];
            sum = sum + mas[i];
        }
        bool flag = false;
        for (int i = 0; i < n; i++)
            if (sum > 1000)
            {
                flag = true;
                break;
            }
        if (flag)
        {
            
            for (int i = 0; i < n-1; i++)
                for (int j = i + 1; j < n ; j++)
                    if (mas[i] > mas[j])
                        std::swap(mas[i], mas[j]);
        }
        Timer t;
      
        for (int i = 0; i < n; i++)
        {
            std::cout << mas[i] << std::endl;
        }
        std::cout << "Time elapsed : " << t.elapsed() << std::endl;
        return 0;


}